﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Arma : MonoBehaviour
{
    public string nombreArma;
    public int tamañoCargador;
    public int cargadoresMaximos;

    internal int cargadorActual;  //el display de la munición tiene que ser cargadorActual/(tamañoCargador * cargadoresRestantes)
    internal int cargadoresRestantes;


    private void Start()
    {
        cargadorActual = tamañoCargador;
        cargadoresRestantes = cargadoresMaximos;
    }

    public void Recargar()
    {
        cargadorActual = tamañoCargador;
        cargadoresRestantes--;
    }

    public abstract IEnumerator Disparar();
}

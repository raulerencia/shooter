﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fusil : Arma
{
    public int dañoBala;
    public int balasPorSegundo;

    public Transform canon;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(Disparar());
        }
        else if (Input.GetMouseButtonUp(0))
        {
            print("Parando corrutina");
            StopAllCoroutines();
        }
    }

    public override IEnumerator Disparar()
    {
        while (true)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                print(hit.transform.name);
            }
            yield return new WaitForSeconds(1/balasPorSegundo);
        }
    }
}
